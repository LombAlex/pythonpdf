## @file globalVariable.py
#
# @section description_globalVariable Global Variable package
# Just a simple variable to know where the user is
# - 0 -> mainView
# - 1 -> mergeView
# - 2 -> rotateView
# - 3 -> splitView

## Initialise function
# 
# Create a global variable called index
# And set it to 0
def init():
    global index
    index = 0