## @file controller.py
#  
# @section description_controller Controller
# File who contain the different function of the program.
#
# @section description_controller_imports Import few library
# This program need PySimpleGUI and PyPDF3
# PySimpleGUI for the GUI
# And PyPDF3 is a tool to modify PDF file

#Imports
from PyPDF3.pdf import PageObject
import Controller.globalVariable
from re import split
import PySimpleGUI as sg
import PyPDF3 as pp3

#Constants
DONE = "Done !"

## Navigate Function
#
#  Function who allow the user to navigate between windows
#  This function check the value of an index (global variable)
# - 0 -> mainView
# - 1 -> mergeView
# - 2 -> rotateView
# - 3 -> splitView
def navigate():
    #Get the current index
    index = Controller.globalVariable.index
    #And import the good view
    if(index == 0):
        from View.mainView import returnView
        return CommonWindow(returnView())

    elif (index == 1):
        from View.mergeView import returnView
        return CommonWindow(returnView())

    elif (index == 2):
        from View.rotateView import returnView
        return CommonWindow(returnView())
    
    elif (index == 3):
        from View.splitView import returnView
        return CommonWindow(returnView())
    
    elif (index == 4):
        from View.multipleView import returnView
        return CommonWindow(returnView())

## Show Slider Function
#
# Allow to display or not a slider.
# Used for the rotation and split windows.
# If the user change the radio button ("each page" or "one page"),
# It display a slider from 1 to the number of page of the doc (input == filename).
# @param window To get the current window
# @param event To get if there's any event
# @param values To get the values array
# @param button1 To get the name of the first radio button
# @param button2 To get the name of the first radio button
# @param input To get the name of the document
# @param slider To get the slider name
def ShowSlider(window, event, values, button1, button2, input, slider):
    #If there's an event on button1, button2 or the input (they changed)
    if event == button1 or event == button2 or event == input:
        #If we don't need to show the slider, then it disapear
        if(values[button1]):
            sliderObj = window.Element(slider)
            sliderObj.update(visible=False)
        else:
            #Ore we try to open it
            try:
                filename = values[input]
                oldFile = open(filename, 'rb')
                reader = pp3.PdfFileReader(oldFile, strict=False)
                #If we can we get the number of page from the doc
                nbPage = reader.getNumPages()
                oldFile.close()
            except:
                #If we can't, we set the number of page to one
                nbPage = 1
            #We display the slider, and change is range
            sliderObj = window.Element(slider)
            sliderObj.update(visible=True, range=(1,nbPage))


##  Check Input Function
#
# Check if all element in the arrays are correct
# These element are input name (like "-INPUT-")
# @param window To get the current window
# @param event To get if there's any event
# @param values To get the values array
# @param arrayEmpty Array who contain element that should not be empty
# @param arrayExist Array who contain element that should exist (as a file)
# @return Boolean, True if all element are correct
def CheckInput(values, arrayEmpty, arrayExist):
    #Check if the element are empty or not
    for input in arrayEmpty:
        if(input == ""):
            return False

    #Check if the element is a real file or not
    for input in arrayExist:
        try:
            filename = values[input]
            oldFile = open(filename, 'rb')
            oldFile.close()
        except:
            return False
    return True


## Common Window Function
#
# All the different window have common code
# So this is where it is located
# @param window and this is the variable of the window (who contain the layout)
def CommonWindow(window):
    loop = True
    while loop:
        #Get the current index, and read the event, values of the window
        index = Controller.globalVariable.index
        event, values = window.read()

        #If the user want to quit, it quit !
        if event == sg.WINDOW_CLOSED or event == 'Quit':
            Controller.globalVariable.index = 9
            break

        #If the user want to go back the mainWindow
        if event == 'Cancel':
            Controller.globalVariable.index = 0
            break
    
        #And we execute the good function, based on the index
        if(index == 0):
            loop = MainWindow(window, event, values)
        elif(index == 1):
            loop = MergeWindow(window, event, values)
        elif(index == 2):
            loop = RotateWindow(window, event, values)
        elif(index == 3):
            loop = SplitWindow(window, event, values)
        elif(index == 4):
            loop = MultipleWindow(window, event, values)
    
    #If we get out of the while, we close the window and the program if the index is 9
    window.close()
    if(index != 9):
        navigate()


## Main Window Function
# This function control the event of the Main View
# @param window To get the current window
# @param event To get if there's any event
# @param values To get the values array
# @return Boolean, if False the program will close the windows
# and open the new window (with the good layout)
def MainWindow(window, event, values):
    #Navigate depending on the button clicked
    if event == "-MERGE-":
        Controller.globalVariable.index = 1
        return False
    elif event == "-ROTATE-":
        Controller.globalVariable.index = 2
        return False
    elif event == "-SPLIT-":
        Controller.globalVariable.index = 3
        return False
    elif event == "-MULTIPLE-":
        Controller.globalVariable.index = 4
        return False
    return True


## Rotate Window Function
#
# This function control the event of the Rotate View
# @param window To get the current window
# @param event To get if there's any event
# @param values To get the values array
# @return True
def RotateWindow(window, event, values):
    #Show the slider if necessary
    ShowSlider(window, event, values, "-RADIOT1-", "-RADIOT2-", "-INPUT-", '-VALT-')

    #Set/Block values of the slider (rotateClockwise want only 90° value)
    if event == "-VAL-":
        val = values["-VAL-"]
        slider = window.Element('-VAL-')
        if(val<=45):
            slider.Update(0)
        elif(val<=135):
            slider.Update(90)
        elif(val<=225):
            slider.Update(180)
        elif(val<=315):
            slider.Update(270)
        else:
            slider.Update(360)

    #if the event is Ok and the input are correct
    elif event == 'Ok' and CheckInput(values, ["-INPUT-", "-OUTPUT-"], ["-INPUT-"]):
        filename = values['-INPUT-']
        oldFile = open(filename, 'rb')
        reader = pp3.PdfFileReader(oldFile, strict=False)
        writer = pp3.PdfFileWriter()
        
        #Get the name of the file (after the last '/')
        name = split("/", filename)[-1]
        newFile = open(values["-OUTPUT-"] + "/" + name, 'wb')

        for page in range(reader.numPages):
            pageObj = reader.getPage(page)
            #If it's for each element or for one element and the current page is the one choose
            if(values["-RADIOT1-"] or values["-RADIOT2-"] and values['-VALT-'] == page-1):
                #Then we rotate the page in the direction wanted
                if(values["-RADIO-"]):
                    pageObj.rotateClockwise(values['-VAL-'])
                else:
                    pageObj.rotateClockwise(-values['-VAL-'])
            writer.addPage(pageObj)
        #Write the file and close them
        writer.write(newFile)
        oldFile.close()
        newFile.close()
        sg.popup(DONE)
    return True

## Merge Window Function
#
# This function control the event of the Merge View
# @param window To get the current window
# @param event To get if there's any event
# @param values To get the values array
# @return True
def MergeWindow(window, event, values):
    #if the event is Ok and the input are correct
    if event == 'Ok' and CheckInput(values, ["-INPUT1-", "-INPUT2-", "-OUTPUT-", "-OUTPUTNAME"], ["-INPUT1-", "-INPUT2-"]):
        filename1 = values['-INPUT1-']
        filename2 = values['-INPUT2-']

        oldFile1 = open(filename1, 'rb')
        oldFile2 = open(filename2, 'rb')
        reader1 = pp3.PdfFileReader(oldFile1, strict=False)
        reader2 = pp3.PdfFileReader(oldFile2, strict=False)
        writer = pp3.PdfFileWriter()
        
        #Prepare the new file
        name = values["-OUTPUTNAME-"]
        newFile = open(values["-OUTPUT-"] + "/" + name + ".pdf", 'wb')
        
        #Add all the page of the first file in the writer
        for page in range(reader1.numPages):
            pageObj = reader1.getPage(page)
            writer.addPage(pageObj)
        #Add all the page of the second file in the writer
        for page in range(reader2.numPages):
            pageObj = reader2.getPage(page)
            writer.addPage(pageObj)
        #Write the file and close them
        writer.write(newFile)
        oldFile1.close()
        oldFile2.close()
        newFile.close()
        sg.popup(DONE)
    return True

## Split Window Function
#
# This function control the event of the Split View
# @param window To get the current window
# @param event To get if there's any event
# @param values To get the values array
# @return True
def SplitWindow(window, event, values):
    #Show the slider if necessary
    ShowSlider(window, event, values, "-RADIO1-", "-RADIO2-", "-INPUT-", '-VAL-')

    #if the event is Ok and the input are correct
    if event == 'Ok' and CheckInput(values, ["-INPUT-", "-OUTPUT-"], ["-INPUT-"]):
        filename = values['-INPUT-']
        oldFile = open(filename, 'rb')
        reader = pp3.PdfFileReader(oldFile, strict=False)
        writers = []
        #Get the name of the file (after the last '/')
        name = split("/", filename)[-1]
        
        for page in range(reader.numPages):
            pageObj = reader.getPage(page)
            #If the user select "each page" or the page is the first one or is the one selected (-VAL-)
            if(values["-RADIO1-"] or page == 0 or page == values["-VAL-"]):
                #We create a new writer and add it to writers (array)
                writer = pp3.PdfFileWriter()
                writer.addPage(pageObj)
                writers.append(writer)
            else:
                #Or we just add the page to the writer, based on if the page is after or before the one selected
                if(page < values["-VAL-"]):
                    writers[0].addPage(pageObj)
                else:
                    writers[1].addPage(pageObj)
        
        name = split("\.", name)[0]
        #Write the files and close them
        for i in range(len(writers)):
            newFile = open(values["-OUTPUT-"] + "/" + name + "-" + str(i+1) + ".pdf", 'wb')
            writers[i].write(newFile)
            newFile.close()
        oldFile.close()
        sg.popup(DONE)
    return True


## Multiple Window Function
#
# This function control the event of the Multiple View
# @param window To get the current window
# @param event To get if there's any event
# @param values To get the values array
# @return True
def MultipleWindow(window, event, values):
    #Set/Block values of the slider (rotateClockwise want only 90° value)
    if event == "-VAL-":
        val = values["-VAL-"]
        slider = window.Element('-VAL-')
        if(val<=45):
            slider.Update(0)
        elif(val<=135):
            slider.Update(90)
        elif(val<=225):
            slider.Update(180)
        elif(val<=315):
            slider.Update(270)
        else:
            slider.Update(360)

    #if the event is Ok and the input are correct
    elif event == 'Ok' and CheckInput(values, ["-INPUT-", "-OUTPUT-"], ["-INPUT-"]):
        filename = values['-INPUT-']
        oldFile = open(filename, 'rb')
        reader = pp3.PdfFileReader(oldFile, strict=False)
        writer = pp3.PdfFileWriter()
        
        #Get the name of the file (after the last '/')
        name = split("/", filename)[-1]
        newFile = open(values["-OUTPUT-"] + "/" + name, 'wb')

        columnNb = values['-VALC-']
        rowNb = values['-VALR-']
        currentColumn = 0
        currentRow = 0
        offsetBetweenRow = 10

        page0 = reader.getPage(0)
        h = page0.mediaBox.getHeight()
        w = page0.mediaBox.getWidth()

        currentPage = pp3.pdf.PageObject.createBlankPage(None, w, h)

        for page in range(reader.numPages):
            pageObj = reader.getPage(page)

            """ #Then we rotate the page in the direction wanted
            if(values["-RADIO-"]):
                pageObj.rotateClockwise(values['-VAL-'])
            else:
                pageObj.rotateClockwise(-values['-VAL-']) """

            if(values["-RADIO-"]):
                pageObj.scaleTo((float)(w)/(float)(columnNb), (float)(h)/(float)(rowNb))
                currentPage.mergeTranslatedPage(pageObj, (float)(currentColumn)*((float)(w)/(float)(columnNb)), -(float)(currentRow-1)*((float)(h)/(float)(rowNb)))
            else:
                pageObj.scaleTo((float)(h)/(float)(rowNb), (float)(w)/(float)(columnNb))
                currentPage.mergeRotatedTranslatedPage(pageObj, (float)(90), (float)(currentColumn)*((float)(w)/(float)(columnNb)), -(float)(currentRow-1)*((float)(h)/(float)(rowNb)))

            currentColumn += 1
            if(currentColumn == columnNb):
                currentColumn = 0
                currentRow += 1
            if(currentRow == rowNb):
                currentRow = 0
                if(page != reader.numPages-1):
                    writer.addPage(currentPage)
                    currentPage = pp3.pdf.PageObject.createBlankPage(None, w, h)

        writer.addPage(currentPage)
        #Write the file and close them
        writer.write(newFile)
        oldFile.close()
        newFile.close()
        sg.popup(DONE)
    return True