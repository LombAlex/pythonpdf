## @mainpage Project PDF
#
#  @section description_main Description
#  This is just few function to easily merge, rotate, split PDF file.
#  (With GUI)
#
# @section notes_main Notes
#  I just wanted to try doxygen, even if with python this is not the best.


## @file main.py
#
#  @section description_main_file Description
#  File to execute, this is the main program code
#
#  This program use the MVC system
#  So there's normally a Model (but i don't use it)
#  A Controller
#  And View

# Imports
import PySimpleGUI as sg
import Controller.globalVariable
from Controller.controller import navigate

## Initialisation
#
#  Set Theme color
#  Initialize the global variable (index to 0)
#  And finally, we execute the navigate function
sg.theme('DarkAmber')
Controller.globalVariable.init()
navigate()
