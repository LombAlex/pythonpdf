## @file splitView.py
# 
# @section description_splitview SplitView
# File who contain the split view of the program

import PySimpleGUI as sg
import os

def returnView():
    currentFolder = os.getcwd()

    layout = [
                [sg.Text("File to split")],
                [
                    sg.Input(key='-INPUT-', enable_events=True),
                    sg.FileBrowse("PDF Files", key='-INPUT-', file_types=(("PDF File", "*.pdf"),), enable_events=True)
                ],
                [
                    sg.Text("Split type"),
                    sg.Radio('Split each page', "-RADIO-", default=True, key='-RADIO1-', enable_events=True),
                    sg.Radio('Split between two page', "-RADIO-", key='-RADIO2-', enable_events=True),
                ],
                [
                    sg.Slider(key="-VAL-", range=(1,1), orientation='h', default_value=1, enable_events=True, visible=False)
                ],
                [sg.Text("Folder where the new file is saved")],
                [
                    sg.Input(default_text=currentFolder, key='-OUTPUT-'),
                    sg.FolderBrowse("Folder", key='-OUTPUT-')
                ],
                [
                    sg.Button('Split the file', key="Ok"),
                    sg.Button('Cancel', key="Cancel"),
                    sg.Button('Quit Application', key="Quit")
                ]
            ]

    window = sg.Window('Window Title', layout)
    return window