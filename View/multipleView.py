## @file multipleView.py
# 
# @section description_multipleview MultipleView
# File who contain the multiple view of the program

import PySimpleGUI as sg
import os

def returnView():
    currentFolder = os.getcwd()
    currentFolder1 = "D:/Programmes Perso/PythonPDF/Test-files/Test4Pages.pdf"
    currentFolder2 = "C:/Users/Alexis/Desktop"
    currentFolder3 = "D:/Programmes Perso/PythonPDF/Test-files"

    layout = [
                [sg.Text("Input file")],
                [
                    sg.Input(key='-INPUT-', default_text=currentFolder1, enable_events=True),
                    sg.FileBrowse("PDF Files", key='-INPUT-', file_types=(("PDF File", "*.pdf"),), initial_folder=currentFolder3, enable_events=True) #laisser initial_folder=currentFolder pour les tests
                ],
                [
                    sg.Text("Direction"),
                    sg.Radio('Original', "-RADIO-", default=True, key='-RADIO-'),
                    sg.Radio('Turned', "-RADIO-"),
                ],
                [
                    sg.Text("How many columns ?"),
                    sg.Slider(key="-VALC-", range=(1,8), orientation='h', default_value=2, enable_events=True)
                ],
                [
                    sg.Text("How many rows ?"),
                    sg.Slider(key="-VALR-", range=(1,8), orientation='h', default_value=2, enable_events=True)
                ],
                [sg.Text("Folder where the new file is saved")],
                [
                    sg.Input(default_text=currentFolder2, key='-OUTPUT-'),
                    sg.FolderBrowse("Folder", key='-OUTPUT-')
                ],
                [
                    sg.Button('Create the file', key="Ok"),
                    sg.Button('Cancel', key="Cancel"),
                    sg.Button('Quit Application', key="Quit")
                ]
            ]

    window = sg.Window('Window Title', layout)
    return window