## @file mainView.py
# 
# @section description_mainview MainView
# File who contain the main view of the program
# Convert ?? don't do any things

import PySimpleGUI as sg

def returnView():
    layout = [
            [sg.Text("Welcome !")],
            [sg.Text("Choose what do you want:")],
            [
                sg.Button('Merge two pdf file', key='-MERGE-'),
                sg.Button('Rotate a pdf file', key='-ROTATE-'),
                sg.Button('Split a pdf file', key='-SPLIT-'),
                sg.Button('Multiple pages on one', key='-MULTIPLE-'),
                sg.Button('Convert ???'),
                sg.Button('Quit')
            ]
        ]

    window = sg.Window('Window Title', layout)
    return window