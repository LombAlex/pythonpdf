## @file mergeView.py
# 
# @section description_mergeview MergeView
# File who contain the merge view of the program

import PySimpleGUI as sg
import os

def returnView():
    currentFolder = os.getcwd()

    layout = [
                [sg.Text("First file to merge")],
                [
                    sg.Input(key='-INPUT1-'),
                    sg.FileBrowse("PDF Files", key='-INPUT1-', file_types=(("PDF File", "*.pdf"),))
                ],
                [sg.Text("Second file to merge")],
                [
                    sg.Input(key='-INPUT2-'),
                    sg.FileBrowse("PDF Files", key='-INPUT2-', file_types=(("PDF File", "*.pdf"),))
                ],
                [sg.Text("Folder where the new file is saved")],
                [
                    sg.Input(default_text=currentFolder, key='-OUTPUT-'),
                    sg.FolderBrowse("Folder", key='-OUTPUT-')
                ],
                [
                    sg.Input(default_text="MergedFile", key='-OUTPUTNAME-'),
                ],
                [
                    sg.Button('Merge the files', key="Ok"),
                    sg.Button('Cancel', key="Cancel"),
                    sg.Button('Quit Application', key="Quit")
                ]
        ]

    window = sg.Window('Window Title', layout)
    return window