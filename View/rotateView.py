## @file rotateView.py
# 
# @section description_rotateview RotateView
# File who contain the rotate view of the program

import PySimpleGUI as sg
import os

def returnView():
    currentFolder = os.getcwd()

    layout = [
                [
                    sg.Text("File to rotate")
                ],
                [
                    sg.Input(key='-INPUT-', enable_events=True),
                    sg.FileBrowse("PDF Files", key='-INPUT-', file_types=(("PDF File", "*.pdf"),))
                ],
                [
                    sg.Text("Rotation value"),
                    sg.Slider(key="-VAL-", range=(0,360), orientation='h', default_value=90, enable_events=True)
                ],
                [
                    sg.Text("Direction"),
                    sg.Radio('Clockwise', "-RADIO-", default=True, key='-RADIO-'),
                    sg.Text("🔁", font=20),
                    sg.Radio('Anticlockwise', "-RADIO-"),
                    sg.Text("🔄", font=20)
                ],
                [
                    sg.Text("Rotate type"),
                    sg.Radio('Rotate each page', "-RADIOT-", default=True, key='-RADIOT1-', enable_events=True),
                    sg.Radio('Rotate one page', "-RADIOT-", key='-RADIOT2-', enable_events=True),
                ],
                [
                    sg.Slider(key="-VALT-", range=(1,1), orientation='h', default_value=1, enable_events=True, visible=False)
                ],
                [
                    sg.Text("Folder where the new file is saved")
                ],
                [
                    sg.Input(default_text=currentFolder, key='-OUTPUT-'),
                    sg.FolderBrowse("Folder", key='-OUTPUT-')
                ],
                [
                    sg.Button('Rotate the file', key="Ok"),
                    sg.Button('Cancel', key="Cancel"),
                    sg.Button('Quit Application', key="Quit")
                ]
            ]

    window = sg.Window('Window Title', layout)
    return window